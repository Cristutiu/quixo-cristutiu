let moveArray = new Array(); // all the squares that get shifted during a single move
let boardArray = new Array(); // will eventually be used for storing a game
const grid = document.querySelector('.wrapper');
const winner = document.querySelector('.winner');
let me = 'X';
var divClass, divStyle, divId;
let selectedRow, selectedColumn;
let opponent, win, hWin, vWin;

// Crearea tablei de joc
function setup (){
    display();

for (o = 0; o < 7; o++) {
    for (x = 0; x < 7; x++) {
        divId = o + '/' + x;
        if (x == 0 || x == 6 || o == 0 || o == 6) {
            divClass = 'surrounding box';
            divStyle = 'visibility: hidden;';
        } else if (x == 1 || x == 5 || o == 1 || o == 5) {
            divClass = 'edge box';
            divStyle = "";
        } else {
            divClass = 'middle box';
            divStyle = "";
        }
        grid.insertAdjacentHTML('beforeend', '<div class ="' + divClass + '" id = "' + divId + '" style = "' + divStyle + '"></div>');
    }
}
grid.addEventListener('click', selectBlockToMove);// make non-opponent edges clickable
}
function display(){

    fill("red");
    circle(600, 450, 850);
}

function announceWinner() {
    winner.innerText = 'Winner: ' + me;
    grid.removeEventListener('click', makeTheMove);
    grid.removeEventListener('click', selectBlockToMove);
}

// determina care boxes poate fi mutata
function selectBlockToMove() {
    let selectedId = event.target.id;
    let boxElement = document.getElementById(selectedId);
    if (boxElement) {
        me == 'X' ? opponent = 'O' : opponent = 'X';
        if (boxElement.className == "edge box" && boxElement.innerText != opponent) {
            hideOldOptions();
            boxElement.style.background = 'green';
            boxElement.style.color = 'white';
            parseInt(selectedRow = selectedId.split("/")[0]);
            parseInt(selectedColumn = selectedId.split("/")[1]);
            surroundingBoxes = [0, 6];
            for (o = 0; o < 2; o++) {
                for (x = 0; x < 2; x++) {
                    if (Math.abs(selectedRow - surroundingBoxes[o]) > 1) {
                        let option = document.getElementById(surroundingBoxes[o] + '/' + selectedColumn);
                        showOption(option);
                    }
                    if (Math.abs(selectedColumn - surroundingBoxes[x]) > 1) {
                        let option = document.getElementById(selectedRow + '/' + surroundingBoxes[x]);
                        showOption(option);
                    }
                }
            }
        }
    }
}

function hideOldOptions() {
    let allSurrounding = document.getElementsByClassName('surrounding');
    let allEdges = document.getElementsByClassName('edge');
    for (i = 0; i < 24; i++) {// hide the surrounding tiles
        allSurrounding[i].style.visibility = 'hidden';
        allSurrounding[i].removeEventListener('click', makeTheMove);
    }
    for (i = 0; i < 16; i++) {//& the edge tile that was previously selected
        allEdges[i].style.background = 'black';
        allEdges[i].style.color = 'white';
    }
}

function showOption(option) {
    option.style.visibility = 'visible';
    option.innerText = '';
    option.addEventListener('mouseover', function(option) {this.innerText = me;});
    option.addEventListener('mouseout', function(option) {this.innerText = '';});
    option.addEventListener('click', makeTheMove);
}

function mouseOver(option){
   option.innerText = me;
}

function makeTheMove(option) {
    let newRow = parseInt(option.srcElement.id.split('/')[0]);
    let newColumn = parseInt(option.srcElement.id.split('/')[1]);
    if (newColumn == selectedColumn) {
        if (newRow > selectedRow) {// shifting sus
            for (i = parseInt(selectedRow); i < parseInt(newRow); i++) {
                document.getElementById(i + '/' + selectedColumn).innerText = document.getElementById((i + 1) + '/' + selectedColumn).innerText;
            }
            document.getElementById((newRow - 1) + '/' + selectedColumn).innerText = me;
        } else {// shifting jos
            for (i = parseInt(selectedRow); i > parseInt(newRow); i--) {
                document.getElementById(i + '/' + selectedColumn).innerText = document.getElementById((i - 1) + '/' + selectedColumn).innerText;
            }
            document.getElementById((newRow + 1) + '/' + selectedColumn).innerText = me;
        }
    } else {// shifting stanga
        if (newColumn > selectedColumn) {
            for (i = parseInt(selectedColumn); i < parseInt(newColumn); i++) {
                document.getElementById(selectedRow + '/' + i).innerText = document.getElementById(selectedRow + '/' + (i + 1)).innerText;
            }
            document.getElementById(selectedRow + '/' + (newColumn - 1)).innerText = me;
        } else {// shifting dreapta
            for (i = parseInt(selectedColumn); i > parseInt(newColumn); i--) {
                document.getElementById(selectedRow + '/' + i).innerText = document.getElementById(selectedRow + '/' + (i - 1)).innerText;
            }
            document.getElementById(selectedRow + '/' + (newColumn + 1)).innerText = me;

        }
    }
    checkForWin(me); // verifica castigator
    checkForWin(opponent); // verifica daca oponentul a castigat
    hideOldOptions(); // ascunde optiunile de mutare dupa ce se realizeaza mutarea
    me == 'X' ? me = 'O' : me = 'X'; // schimba jucatori
    document.querySelector('.player').innerText = 'Next Play: ' + me; // displays player
}
// Verifica castigatorul
function checkForWin(me) {
    makeBoardIntoArray();
    for (o = 1; o < 6; o++) {
        hWin = 0;
        win = false;
        for (x = 1; x < 6; x++) {
            boardArray[o + '/' + x] == me ? hWin++ : hWin = 0;
            hWin == 5 ? win = true : win;
        }
        win == true ? announceWinner() : win;
    }
    for (x = 1; x < 6; x++) {
        vWin = 0;
        win = false;
        for (o = 1; o < 6; o++) {
            boardArray[o + '/' + x] == me ? vWin++ : vWin = 0;
            vWin == 5 ? win = true : win;
        }
        win == true ? announceWinner() : win;
    }
    if (boardArray['1/1'] == me &&
        boardArray['2/2'] == me &&
        boardArray['3/3'] == me &&
        boardArray['4/4'] == me &&
        boardArray['5/5'] == me) {
        announceWinner();
    }
    if (boardArray['1/5'] == me &&
        boardArray['2/4'] == me &&
        boardArray['3/3'] == me &&
        boardArray['4/2'] == me &&
        boardArray['5/1'] == me) {
        announceWinner();
    }
}

function makeBoardIntoArray() {// an array containing the state of each square on the board to facilitate storing a game's state someday.
    for (o = 1; o < 6; o++) {
        for (x = 1; x < 6; x++) {
            boardArray[o + '/' + x] = document.getElementById(o + '/' + x).innerText;
        }
    }
}