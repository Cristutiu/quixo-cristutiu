let moveArray = new Array(); 
let boardArray = new Array();
const grid = document.querySelector('.wrapper');
const winner = document.querySelector('.winner');
//let restartButton = getElementById('restartButton')
let me = 'X';
var divClass, divStyle, divId;
let selectedRow, selectedColumn;
let opponent, win, hWin, vWin;

// Crearea tablei de joc


function setup () {
    
    canvas = createCanvas(windowWidth, windowHeight);
    canvas.position(0,0);
    canvas.style('z-index', '-1');

for (o = 0; o < 7; o++) {
    for (x = 0; x < 7; x++) {
        divId = o + '/' + x;
        if (x == 0 || x == 6 || o == 0 || o == 6) {
            divClass = 'surrounding box';
            divStyle = 'visibility: hidden;';
        } else if (x == 1 || x == 5 || o == 1 || o == 5) {
            divClass = 'edge box';
            divStyle = "";
        } else {
            divClass = 'middle box';
            divStyle = "";
        }
        grid.insertAdjacentHTML('beforeend', '<div class ="' + divClass + '" id = "' + divId + '" style = "' + divStyle + '"></div>');
    }
}
inputNameP1();
//inputNameP2();
grid.addEventListener('click', selectBlockToMove);
}





// determina care boxes poate fi mutata
function selectBlockToMove() {
    let selectedId = event.target.id;
    let boxElement = document.getElementById(selectedId);
    if (boxElement) {
        me == 'X' ? opponent = 'O' : opponent = 'X';
        if (boxElement.className == "edge box" && boxElement.innerText != opponent) {
            hideOldOptions();
            boxElement.style.background = 'green';
            boxElement.style.color = 'purple';
            parseInt(selectedRow = selectedId.split("/")[0]);
            parseInt(selectedColumn = selectedId.split("/")[1]);
            surroundingBoxes = [0, 6];
            for (o = 0; o < 2; o++) {
                for (x = 0; x < 2; x++) {
                    if (Math.abs(selectedRow - surroundingBoxes[o]) > 1) {
                        let option = document.getElementById(surroundingBoxes[o] + '/' + selectedColumn);
                        showOption(option);
                    }
                    if (Math.abs(selectedColumn - surroundingBoxes[x]) > 1) {
                        let option = document.getElementById(selectedRow + '/' + surroundingBoxes[x]);
                        showOption(option);
                    }
                }
            }
        }
    }
}

function hideOldOptions() {
    let allSurrounding = document.getElementsByClassName('surrounding');
    let allEdges = document.getElementsByClassName('edge');
    for (i = 0; i < 24; i++) {
        allSurrounding[i].style.visibility = 'hidden';
        allSurrounding[i].removeEventListener('click', makeTheMove);
    }
    for (i = 0; i < 16; i++) {
        allEdges[i].style.background = 'black';
        allEdges[i].style.color = 'white';
    }
}

// INputs for Player 1
function inputNameP1() {
    numePlayerOne = createInput();
    numePlayerOne.position(20,65);

    butonPlayerOne = createButton('submit');
    butonPlayerOne.position(numePlayerOne.x + numePlayerOne.width, 65);
    butonPlayerOne.mousePressed(nameP1);
    textNume = createElement('h2', 'what is your name?');
    textNume.position(20, 5);
}

function nameP1() {
const nume = numePlayerOne.value();
textNume.html('Player 1:' + nume);
numePlayerOne.value('');
}

// Inputs for Playes 2 

// function inputNameP2() {
//     numePlayerTwo = createInput();
//     numePlayerTwo.position(20, 140);

//     butonPlayerTwo = createButton('submit');
//     butonPlayerTwo.position(numePlayerTwo.x + numePlayerTwo.width, 140);
//     buttonPlayerTwo.mousePressed(nameP2);
//     textNume = createElement('h2', 'what is your name?');
//     textNume.position(20, 120); 
// }

// function nameP2() {
//     const nume = numePlayerTwo.value();
//     textNume.html('Player 2:' + nume);
//     numePlayerTwo.value('');
// }

function showOption(option) {
    option.style.visibility = 'visible';
    option.innerText = '';
    option.addEventListener('mouseover', function(option) {this.innerText = me;});
    option.addEventListener('mouseout', function(option) {this.innerText = '';});
    option.addEventListener('click', makeTheMove);
}

function mouseOver(option){
   option.innerText = me;
}

function makeTheMove(option) {
    let newRow = parseInt(option.srcElement.id.split('/')[0]);
    let newColumn = parseInt(option.srcElement.id.split('/')[1]);
    if (newColumn == selectedColumn) {
        if (newRow > selectedRow) {// shifting sus
            for (i = parseInt(selectedRow); i < parseInt(newRow); i++) {
                document.getElementById(i + '/' + selectedColumn).innerText = document.getElementById((i + 1) + '/' + selectedColumn).innerText;
            }
            document.getElementById((newRow - 1) + '/' + selectedColumn).innerText = me;
        } else {// shifting jos
            for (i = parseInt(selectedRow); i > parseInt(newRow); i--) {
                document.getElementById(i + '/' + selectedColumn).innerText = document.getElementById((i - 1) + '/' + selectedColumn).innerText;
            }
            document.getElementById((newRow + 1) + '/' + selectedColumn).innerText = me;
        }
    } else {// shifting stanga
        if (newColumn > selectedColumn) {
            for (i = parseInt(selectedColumn); i < parseInt(newColumn); i++) {
                document.getElementById(selectedRow + '/' + i).innerText = document.getElementById(selectedRow + '/' + (i + 1)).innerText;
            }
            document.getElementById(selectedRow + '/' + (newColumn - 1)).innerText = me;
        } else {// shifting dreapta
            for (i = parseInt(selectedColumn); i > parseInt(newColumn); i--) {
                document.getElementById(selectedRow + '/' + i).innerText = document.getElementById(selectedRow + '/' + (i - 1)).innerText;
            }
            document.getElementById(selectedRow + '/' + (newColumn + 1)).innerText = me;

        }
    }
    checkForWin(me); // verifica castigator
    checkForWin(opponent); // verifica daca oponentul a castigat
    hideOldOptions(); // ascunde optiunile de mutare dupa ce se realizeaza mutarea
    me == 'X' ? me = 'O' : me = 'X'; // schimba jucatori
    document.querySelector('.player').innerText = 'Next Play: ' + me; // displays player
}
// Verifica castigatorul
function checkForWin(me) {
    makeBoardIntoArray();
    for (o = 1; o < 6; o++) {
        hWin = 0;
        win = false;
        for (x = 1; x < 6; x++) {
            boardArray[o + '/' + x] == me ? hWin++ : hWin = 0;
            hWin == 5 ? win = true : win;
        }
        win == true ? announceWinner() : win;
    }
    for (x = 1; x < 6; x++) {
        vWin = 0;
        win = false;
        for (o = 1; o < 6; o++) {
            boardArray[o + '/' + x] == me ? vWin++ : vWin = 0;
            vWin == 5 ? win = true : win;
        }
        win == true ? announceWinner() : win;
    }
    if (boardArray['1/1'] == me &&
        boardArray['2/2'] == me &&
        boardArray['3/3'] == me &&
        boardArray['4/4'] == me &&
        boardArray['5/5'] == me) {
        announceWinner();
    }
    if (boardArray['1/5'] == me &&
        boardArray['2/4'] == me &&
        boardArray['3/3'] == me &&
        boardArray['4/2'] == me &&
        boardArray['5/1'] == me) {
        announceWinner();
    }
}

function announceWinner() {
    winner.innerText = 'Winner: ' + me;
    grid.removeEventListener('click', makeTheMove);
    grid.removeEventListener('click', selectBlockToMove);
}

function makeBoardIntoArray() {// an array containing the state of each square on the board to facilitate storing a game's state someday.
    for (o = 1; o < 6; o++) {
        for (x = 1; x < 6; x++) {
            boardArray[o + '/' + x] = document.getElementById(o + '/' + x).innerText;
        }
    }
}