let me = 'X'; // X always starts, but then this variable alternates betweed X and O
let moveArray = new Array(); // all the squares that get shifted during a single move
let boardArray = new Array(); // will eventually be used for storing a game
const grid = document.querySelector('.wrapper');
const winner = document.querySelector('.winner');

var divClass, divStyle, divId, selectedRow, selectedColumn, opponent, win, hWin, vWin;

for (r = 0; r < 7; r++) {// build the initial grid
    for (x = 0; x < 7; x++) {
        divId = r + '/' + x;
        if (x == 0 || x == 6 || r == 0 || r == 6) {
            divClass = 'surrounding box';
            divStyle = 'visibility: hidden;';
        } else if (x == 1 || x == 5 || r == 1 || r == 5) {
            divClass = 'edge box';
            divStyle = "";
        } else {
            divClass = 'middle box';
            divStyle = "";
        }
        grid.insertAdjacentHTML('beforeend', '<div class ="' + divClass + '" id = "' + divId + '" style = "' + divStyle + '"></div>');
    }
}
grid.addEventListener('click', selectBlockToMove);// make non-opponent edges clickable

function announceWinner() {
    winner.innerText = 'Winner: ' + me;
    grid.removeEventListener('click', makeTheMove);
    grid.removeEventListener('click', selectBlockToMove);
}

function selectBlockToMove() {
    //selectBlockToMove = function () { //determine which boxes can be moved
    let selectedId = event.target.id;
    let boxElement = document.getElementById(selectedId);
    if (boxElement) {
        me == 'X' ? opponent = 'O' : opponent = 'X';
        if (boxElement.className == "edge box" && boxElement.innerText != opponent) {
            hideOldOptions();
            boxElement.style.background = 'white';
            boxElement.style.color = 'white';
            parseInt(selectedRow = selectedId.split("/")[0]);
            parseInt(selectedColumn = selectedId.split("/")[1]);
            surroundingBoxes = [0, 6];
            for (r = 0; r < 2; r++) {
                for (x = 0; x < 2; x++) {
                    if (Math.abs(selectedRow - surroundingBoxes[r]) > 1) {
                        let option = document.getElementById(surroundingBoxes[r] + '/' + selectedColumn);
                        showOption(option);
                    }
                    if (Math.abs(selectedColumn - surroundingBoxes[x]) > 1) {
                        let option = document.getElementById(selectedRow + '/' + surroundingBoxes[x]);
                        showOption(option);
                    }
                }
            }
        }
    }
}

function hideOldOptions() {
    let allSurrounding = document.getElementsByClassName('surrounding');
    let allEdges = document.getElementsByClassName('edge');
    for (i = 0; i < 24; i++) {// hide the surrounding tiles
        allSurrounding[i].style.visibility = 'hidden';
        allSurrounding[i].removeEventListener('click', makeTheMove);
    }
    for (i = 0; i < 16; i++) {//& the edge tile that was previously selected
        allEdges[i].style.background = 'black';
        allEdges[i].style.color = 'white';
    }
}

function showOption(option) {
    option.style.visibility = 'visible';
    option.innerText = '';
    option.addEventListener('mouseover', function(option) {this.innerText = me;});
    option.addEventListener('mouseout', function(option) {this.innerText = '';});
    option.addEventListener('click', makeTheMove);
}

function mouseOver(option){
   option.innerText = me;
}

function makeTheMove(option) {// different ways the tiles/boxes can shift
    let newRow = parseInt(option.srcElement.id.split('/')[0]);
    let newColumn = parseInt(option.srcElement.id.split('/')[1]);
    if (newColumn == selectedColumn) {
        if (newRow > selectedRow) {// code for shifting tiles UP
            for (i = parseInt(selectedRow); i < parseInt(newRow); i++) {
                document.getElementById(i + '/' + selectedColumn).innerText = document.getElementById((i + 1) + '/' + selectedColumn).innerText;
            }
            document.getElementById((newRow - 1) + '/' + selectedColumn).innerText = me;
        } else {// code for shifting tiles DOWN
            for (i = parseInt(selectedRow); i > parseInt(newRow); i--) {
                document.getElementById(i + '/' + selectedColumn).innerText = document.getElementById((i - 1) + '/' + selectedColumn).innerText;
            }
            document.getElementById((newRow + 1) + '/' + selectedColumn).innerText = me;
        }
    } else {// code for shifting tiles LEFT
        if (newColumn > selectedColumn) {
            for (i = parseInt(selectedColumn); i < parseInt(newColumn); i++) {
                document.getElementById(selectedRow + '/' + i).innerText = document.getElementById(selectedRow + '/' + (i + 1)).innerText;
            }
            document.getElementById(selectedRow + '/' + (newColumn - 1)).innerText = me;
        } else {// code for shifting tiles RIGHT
            for (i = parseInt(selectedColumn); i > parseInt(newColumn); i--) {
                document.getElementById(selectedRow + '/' + i).innerText = document.getElementById(selectedRow + '/' + (i - 1)).innerText;
            }
            document.getElementById(selectedRow + '/' + (newColumn + 1)).innerText = me;

        }
    }
    checkForWin(me); // see if player won
    checkForWin(opponent); // see if player caused opponent to win
    hideOldOptions(); // hides move options once a player makes a move
    me == 'X' ? me = 'O' : me = 'X'; // switches player
    document.querySelector('.player').innerText = 'Next Play: ' + me; // displays player
}

function checkForWin(me) {
    makeBoardIntoArray();
    for (r = 1; r < 6; r++) {//check for horizontal wins
        hWin = 0;
        win = false;
        for (x = 1; x < 6; x++) {
            boardArray[r + '/' + x] == me ? hWin++ : hWin = 0;
            hWin == 5 ? win = true : win;
        }
        win == true ? announceWinner() : win;
    }
    for (x = 1; x < 6; x++) {//check for vertical wins
        vWin = 0;
        win = false;
        for (r = 1; r < 6; r++) {
            boardArray[r + '/' + x] == me ? vWin++ : vWin = 0;
            vWin == 5 ? win = true : win;
        }
        win == true ? announceWinner() : win;
    }
    if (boardArray['1/1'] == me &&//check for one diagonal win
        boardArray['2/2'] == me &&
        boardArray['3/3'] == me &&
        boardArray['4/4'] == me &&
        boardArray['5/5'] == me) {
        announceWinner();
    }
    if (boardArray['1/5'] == me &&//check for second diagonal win
        boardArray['2/4'] == me &&
        boardArray['3/3'] == me &&
        boardArray['4/2'] == me &&
        boardArray['5/1'] == me) {
        announceWinner();
    }
}

function makeBoardIntoArray() {// an array containing the state of each square on the board to facilitate storing a game's state someday.
    for (r = 1; r < 6; r++) {
        for (x = 1; x < 6; x++) {
            boardArray[r + '/' + x] = document.getElementById(r + '/' + x).innerText;
        }
    }
}