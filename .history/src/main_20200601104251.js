const cellElements = document.querySelectorAll('[data-cell]')
const board = document.getElementById('board')
const winningMessageElement = document.getElementById('winningMessage')
const winningMessageTextElement = document.querySelector('[data-winning-message-text')
const restartButton = document.getElementById('restartButton')
const X_CLASS = 'x';
const CIRCLE_CLASS = 'circle';
let circleTurn;
var input1;
var input2;
let button1;
let button2;
var greeting1;
var greeting2;



 draw();

 restartButton.addEventListener('click', draw)


function draw() {
    circleTurn = false;
    cellElements.forEach(cell => {
        // Restart function
        cell.classList.remove(X_CLASS)
        cell.classList.remove(CIRCLE_CLASS)
        cell.removeEventListener('click', setup)
        //End's here  
        cell.addEventListener('click', setup, {once: true})
    })
    setBoardsHoverClass()
    winningMessageElement.classList.remove('show')

}


function setup(e) {
    inputs.input()
  //  createCanvas(200,200)
    const cell = e.target
    const currentClass = circleTurn ? CIRCLE_CLASS : X_CLASS
    placeMark(cell, currentClass)
    if(checkWin(currentClass)) {
        endGame(false)
    }else if(isDraw()) {
        endGame(true)
    }else {
    swapTurns()
    setBoardsHoverClass()
    }
}


class Input {
    input() {
        input1 = createInput("Player1")
        input2 = createInput("Player2")
        input1.position(10,40)
        // text("Insert your name", 10, 10)
        // fill('white')
        input2.position(1300,40)
        // text("Insert your name", 1300, 60)
        // fill('white')
        button1 = createButton('submit')
        button1.position(170,40)
        button1.mousePressed(inputs.greet())
        button2 = createButton('submit')
        button2.position(1245, 40)
        button2.mousePressed(inputs.greet())

        // greeting1 = createElemenet('h2', 'Your name')
        // greeting1.position(10, 60)
        // greeting2 = createElement('h2', 'Your name')
        // greeting2.position(1250,50) 
    }
    greet(name1, name2) {
        name1 = input1.value()
        text(name1)
        name2 = input2.value()
        text(name2)
    }
}
var inputs = new Input();


function endGame(equal) {
    if(equal) {
        winningMessageTextElement.innerText = 'Draw!'
    }
    else {
        winningMessageTextElement.innerText = `${circleTurn ? "O's" : "X's"} Wins!`
    }
    winningMessageElement.classList.add('show')
}

function isDraw() {
    return [...cellElements].every(cell => {
        return cell.classList.contains(X_CLASS) || 
        cell.classList.contains(CIRCLE_CLASS)
    })
}

// Now I can add forms :D
function placeMark(cell, currentClass) {
    cell.classList.add(currentClass)
}

// Now I can add circles too :D

function swapTurns() {
    circleTurn = !circleTurn;
}

function setBoardsHoverClass() {
    board.classList.remove(X_CLASS)
    board.classList.remove(CIRCLE_CLASS)
    if(circleTurn) {
        board.classList.add(CIRCLE_CLASS)
    }else {
        board.classList.add(X_CLASS)
    }
}
// Checking the winner if he/she/it's alright
function checkWin(currentClass) {
    WINNING_COMBINATIONS.some(combination => {
        return combination.every(index => {
            return cellElements[index].classList.contains(currentClass)
        })
    })
}



